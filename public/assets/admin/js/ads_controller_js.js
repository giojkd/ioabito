$(document).ready(function () {
    var new_event_autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('p1_full_address')),
            {types: []});
    google.maps.event.addListener(new_event_autocomplete, 'place_changed', function () {
        var place = new_event_autocomplete.getPlace();
        var result = place.address_components;
        var results = new Array();
        if (result.length > 0) {
            for (var item in result) {
                var types = result[item].types
                for (var item2 in types) {
                    results[types[item2]] = result[item].long_name;
                }
            }
            results['lat'] = place.geometry.location.lat();
            results['lng'] = place.geometry.location.lng();
            console.log(results);
            for (var i in results) {
                $('input[name="p1_' + i + '"]').val(results[i]);
            }
        }

    });


    var new_event_autocomplete2 = new google.maps.places.Autocomplete(
            (document.getElementById('p2_full_address')),
            {types: []});
    google.maps.event.addListener(new_event_autocomplete, 'place_changed', function () {
        var place = new_event_autocomplete.getPlace();
        var result = place.address_components;
        var results = new Array();
        if (result.length > 0) {
            for (var item in result) {
                var types = result[item].types
                for (var item2 in types) {
                    results[types[item2]] = result[item].long_name;
                }
            }
            results['lat'] = place.geometry.location.lat();
            results['lng'] = place.geometry.location.lng();
            console.log(results);
            for (var i in results) {
                $('input[name="p2_' + i + '"]').val(results[i]);
            }
        }

    });
})

