

loadHomePage();

var filters = {};

/*
 ****************************
 *On Page Callbacks
 ****************************
 */
pageCallbacks.home = function (page) {

}

pageCallbacks.home_scheduloo_mods = function (page) {
    var calendarDateFormat = app.calendar.create({
        inputEl: '#demo-calendar-date-format',
        dateFormat: 'M dd yyyy',
        closeOnSelect:true,
        yearSelector:true
    });
}


var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August' , 'September' , 'October', 'November', 'December'];

var datepickerToolbar = '<div id="yearsList">';

for(var i = 2002; i>= 1930; i--){
       datepickerToolbar+= '<a class="calendarYearPicker" id="year'+i+'" onclick="selectYear('+i+')">'+i+'</a>'; 
}
datepickerToolbar+='</div>';
datepickerToolbar += '<div id="monthsList">';

for(var i in monthNames){
    datepickerToolbar+= '<a class="calendarMonthPicker" id="month'+i+'" data-month="" href="javascript:" onclick="selectMonth('+i+')">' + monthNames[i] + '</a>';
}
datepickerToolbar+= '</div>';


var cdf;
var selectedYear = 2002;
var selectedMonth = 11;

function selectMonth(month){
    $$('.calendarMonthPicker').removeClass('active');
    $$('#month'+month).addClass('active');
    selectedMonth = month;
    setDate();
}
function selectYear(year){
    $$('.calendarYearPicker').removeClass('active');
    $$('#year'+year).addClass('active');
    selectedYear = year;
}

function setDate(){
    cdf.setYearMonth(selectedYear,selectedMonth,300)
}

pageCallbacks.register1_scheduloo = function (page) {
     cdf = app.calendar.create({
        inputEl: '#demo-calendar-date-format',
        dateFormat: 'dd-mm-yyyy',
        closeOnSelect:true,
        yearSelector:true,
        renderToolbar: function(){
            return datepickerToolbar;
        },
        on:{
            opened:function(){
                    selectYear(selectedYear);
                    selectMonth(selectedMonth);
            }
        }
    });
}















function loadResultCategories(){
    var filters = {ad_categories_id:window.localStorage.getItem('ad_categories_id')};
    app.request.get(api_endpoint+'/api/categories',filters,function(data){
        data = JSON.parse(data);
        var context = {
            categories:data
        }
        var compiledTemplate2 = Template7.compile($$('#categories-list-template').html());  
         $$('#categories-list').html(compiledTemplate2(context));
           $$('#results-list-filters-form-submit').on('click',function(){
            var formData = app.form.convertToData('#results-list-filters-form');
            loadResults(formData);
            
        })
    })
      
}


function loadCategories(){
     app.request.get(api_endpoint+'/api/categories',{ad_categories_id:window.localStorage.getItem('ad_categories_id')},function(data){
        data = JSON.parse(data);
        var context = {
            categories:data,
            cms_users_id:window.localStorage.getItem('cms_user_id'),
        }
        var compiledTemplate = Template7.compile($$('#item-insert-form-template').html());  
        $$('#item-insert-form').html(compiledTemplate(context));
        $$('#item-insert-form-template').remove();
        $$('select[name="ad_categories_id"]').change(function(){
            $$(this).find('option').each(function(){
                if($$(this).is(':checked')){
                    if(parseInt($$(this).attr('categories_id'))==1){
                        $$('#p2_address').show();
                        $$('#item-insert-label-from').html('Da');
                    }else{
                        $$('#p2_address').hide();
                        $$('#item-insert-label-from').html('Indirizzo');
                    }
                }
            })
        });
        $$('#new-item-form-submit').on('click',function(){
                    var formData = app.form.convertToData('#new-item-form');
                    console.log(formData);
                     app.request.post(api_endpoint+'/api/ads_add_create',formData,function(data){
                        data = JSON.parse(data);
                        if(data.id>0){
                            mainView.router.navigate('/item-insert-form-thank-you/');
                        }else{
                            alert(data.api_message);
                        }
            })
        })

         $$('body').on('touchend','.pac-container', function(e){e.stopImmediatePropagation();})
                    var address1 = document.getElementById('address1');
                    var autocomplete1 = new google.maps.places.Autocomplete(address1);
                    var address2 = document.getElementById('address2');
                    var autocomplete2 = new google.maps.places.Autocomplete(address2);

    })
}

function loadResult(id){
    app.request.get(api_endpoint+'/api/ads',{ads_id:id,cms_users_id_geo:window.localStorage.getItem('cms_user_id')},function(data){
        data = JSON.parse(data);
        var context = {
            result:data.results[0]
        }
        var item = data.results[0];
        
         var compiledTemplate = Template7.compile($$('#result-page-template').html());  
         $$('#result-page').html(compiledTemplate(context));

         $$('#phoneCall').attr('href','tel:'+item.telephone);
         $$('#chatHref').attr('href','/chat/'+id+'/0/');

         if(item.cms_users_id == window.localStorage.getItem('cms_user_id')){
            $$('#toolbarBottom').remove();
         }
         
         console.log(item);

        var map_results_center = {lat:43.769562,lng:11.255814};
        var map_product =  new google.maps.Map(document.getElementById('map-product'), {zoom: 12, center: map_results_center});
        new google.maps.Marker({position: {lat:parseFloat(item.p1_lat),lng:parseFloat(item.p1_lng)}, map: map_product})
    })
}


function loadResults(filters){
     filters = (typeof(filters) != 'undefined') ? filters:{};

     filters.cms_users_id_geo = window.localStorage.getItem('cms_user_id');

     app.request.get(api_endpoint+'/api/ads',filters,function(data){
        data = JSON.parse(data);
        var context = {
            resultsFound:data.resultsFound,
            results:data.results,
            categories:data.categories
        }
        var compiledTemplate = Template7.compile($$('#results-list-template').html());  
        $$('#results-list').html(compiledTemplate(context));

        app.tab.show('#tab1', true);                

        var map_results_center = {lat:43.769562,lng:11.255814};
        var map_results =  new google.maps.Map(document.getElementById('map-results'), {zoom: 12, center: map_results_center});
        
        var markers = new Array();

        for(var i in context.results){
            var item = context.results[i];
            var position = {lat:parseFloat(item.p1_lat),lng:parseFloat(item.p1_lng)};
            
            markers.push(new google.maps.Marker({position: position, map: map_results, id: item.id}));
            for(var j in markers){
                 markers[j].addListener('click', function() {
                    mainView.router.navigate('/product-page/'+this.id+'/');
                });
            }
        }
    })
}

function loadHomePage() {
     var cms_user_id = window.localStorage.getItem('cms_user_id');
        if(cms_user_id>0){
            mainView.router.navigate('/results-list/');
        }else{
            mainView.router.navigate('/home/');
        }
}