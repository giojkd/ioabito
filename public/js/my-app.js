var $$ = Dom7;

// Let's register Template7 helper so we can pass json string in links
Template7.registerHelper('json_stringify', function (context) {
    return JSON.stringify(context);
});

var api_endpoint = 'http://localhost:8000';

function setToolbarColor(){
                            var toolbarColors =[];
                            toolbarColors[1] = 'orange';
                            toolbarColors[2] = 'fuxia';
                            toolbarColors[3] = 'green';
                            toolbarColors[4] = 'blue';
                            toolbarColors[5] = 'violet';
                            var ad_categories_id = window.localStorage.getItem('ad_categories_id');
                            var toolbarClass = 'toolbar-'+toolbarColors[ad_categories_id];
                            console.log(toolbarClass);
                            $$('.resultsListToolbar').addClass(toolbarClass);
                    }

var pageCallbacks = {};
// Initialize your app
var app = new Framework7({
    root: '#app',
    name: 'Scheduloo App',
    id: 'com.icteuro.scheduloo',
    // Add default routes
    routes: [
        { 
            path:'/text_content/:id/',
            url:'text_content.html',
            name:'text_content',
            on:{
                pageAfterIn:function(event,page){
                    var params = page.route.params; 
                     app.request.get(api_endpoint+'/api/text_contents',{id:params.id},function(data){
                        var data = JSON.parse(data);
                        var context = data.data[0]; 
                        var compiledTemplate = Template7.compile($$('#textContentTemplate').html());  
                        $$('#textContent').html(compiledTemplate(context));
                    })
                }
            }
        },
        {
            path:'/chats-list/',
            url:'chats-list.html',
            on:{
                pageAfterIn:function(event,page){
                      app.request.get(api_endpoint+'/api/chat-list/'+window.localStorage.getItem('cms_user_id'),{},function(data){
                        var data = JSON.parse(data);
                        var context = {}; 
                        context.results = data;
                        var compiledTemplate = Template7.compile($$('#chats-list-template').html());  
                        $$('#chats-list').html(compiledTemplate(context));
                    })
                }
            }
        },
        {
            path:'/chat/:ads_id/:cms_users_id/',
            url:'chat.html',
            name:'chat',
            on:{
                pageAfterIn:function(event,page){
                var params = page.route.params; 
                var ads_id = params.ads_id;
                var cms_users_id = (params.cms_users_id>0) ? params.cms_users_id : window.localStorage.getItem('cms_user_id');
                var cms_users_id_msg = window.localStorage.getItem('cms_user_id');

                app.request.get(api_endpoint+'/api/get-chats-id/'+ads_id+'/'+cms_users_id+'/',function(chats_id){
                        var chats_id = chats_id;
                        app.request.get(api_endpoint+'/api/get-chat-messages/'+chats_id+'/'+cms_users_id_msg+'/',function(data){
                            var oldMessages = JSON.parse(data);
                            var context = {messages:oldMessages};
                            var compiledTemplate = Template7.compile($$('.messages-template').html());  
                            $$('.messages').html(compiledTemplate(context));
                            $$('.messages-template').remove();
                            const pusher = new Pusher('b3a0d0d3965356c99245', {
                            cluster: 'eu',
                            encrypted: true,
                            });
                            const channel = pusher.subscribe('chat');
                            channel.bind('chat_'+chats_id, data => {
                                console.log(data);
                                  if(data.cms_users_id != cms_users_id_msg){
                                        messages.addMessage({
                                        text: data.text,
                                        type: 'received',
                                        name: data.name
                                      });
                                     cordova.plugins.notification.local.schedule([
                                            { id: 1, title: 'My first notification' },
                                            { id: 2, title: 'My first notification' }
                                    ]);   
                                  }

                            });

/***********************/


                    var messages = app.messages.create({
                      el: '.messages',
                      firstMessageRule: function (message, previousMessage, nextMessage) {
                        if (message.isTitle) return false;
                        if (!previousMessage || previousMessage.type !== message.type || previousMessage.name !== message.name) return true;
                        return false;
                      },
                      lastMessageRule: function (message, previousMessage, nextMessage) {
                        if (message.isTitle) return false;
                        if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
                        return false;
                      },
                      tailMessageRule: function (message, previousMessage, nextMessage) {
                        if (message.isTitle) return false;
                        if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
                        return false;
                      }
                    });
                    var messagebar = app.messagebar.create({
                      el: '.messagebar'
                    });
                    $$('.send-link').on('click', function () {
                      var text = messagebar.getValue().replace(/\n/g, '<br>').trim();
                      if (!text.length) return;
                      messagebar.clear();
                      messagebar.focus();
                       app.request.post(api_endpoint+'/api/new-message',
                       {
                        message:text,
                        cms_users_id:cms_users_id,
                        cms_users_id_msg:cms_users_id_msg,
                        ads_id:ads_id
                        },function(data){
                            data = JSON.parse(data);
                        })
                          messages.addMessage({
                            text: text,
                          });
                            });
/***********************/
                        })
                        
                    })

                }
            }
        },
        {
            path:'/categories/',
            url:'categories.html',
            on:{
                pageAfterIn:function(event,page){

                }
            }
        },
        {
            path:'/sign-in/',
            url:'sign-in.html',
            on:{
                pageAfterIn:function(event,page){
                    $$('#sign-in-submit').on('click',function(){
                        var telephone = $$('input[name="telephone"]').val();
                        if(telephone==''){
                            alert('Il numero inserito non è valido.');
                        }else{
                            app.request.post(api_endpoint+'/api/sign-in-user',{telephone:telephone},function(data){
                                data = JSON.parse(data);
                                if(data.status==1){
                                    mainView.router.navigate('/sign-up-confirm/',{
                                    context:{
                                        telephone:telephone
                                        }
                                    });
                                }else{  
                                    alert(data.message);
                                }
                            })
                            
                            
                        }
                    })
                }
            }
        },
        {
            path:'/user/',
            url:'user.html',
            on:{
                pageAfterIn:function(event,page){
                    app.request.get(api_endpoint+'/api/users_get',{id:window.localStorage.getItem('cms_user_id')},function(data){
                        var data = JSON.parse(data);
                        var context = data.data[0];
                        var compiledTemplate = Template7.compile($$('#update-user-form-template').html());  
                        $$('#update-user-form').html(compiledTemplate(context));
                        $$('#update-user-form-template').remove();
                        var calendarDateFormat = app.calendar.create({
                            inputEl: '#demo-calendar-date-format',
                            dateFormat: 'dd-mm-yyyy'
                        });
                        $$('#user-update-form-submit').on('click',function(){
                           var formData = app.form.convertToData('#user-update-form');
                           console.log(formData);
                                  app.request.post(api_endpoint+'/api/users_update',formData,function(data){
                                        data = JSON.parse(data);
                                        if(data.api_status==1){
                                            var toastIcon = app.toast.create({
                                                  icon: app.theme === 'ios' ? '<i class="f7-icons">check</i>' : '<i class="material-icons">check</i>',
                                                  text: 'Impostazioni salvate',
                                                  position: 'center',
                                                  closeTimeout: 2000,
                                            });
                                            toastIcon.open();
                                        }else{

                                        }
                                  })
                        })
                        $$('#sign-out-submit').on('click',function(){
                          localStorage.removeItem('cms_user_id');
                          mainView.router.navigate('/home/');
                        })
                    })
                }
            }
        },
        {
            path:'/home/',
            url: 'home.html',
        },
        {
            path: '/sign-up/',
            url: 'sign-up.html',
            on:{
                pageAfterIn:function(event,page){
                    $$('body').on('touchend','.pac-container', function(e){e.stopImmediatePropagation();})
                    var address1 = document.getElementById('address1');
                    var autocomplete1 = new google.maps.places.Autocomplete(address1);
                }
            }
        },
        {
            path: '/sign-up-confirm/',
            templateUrl: 'sign-up-confirm.html',
            on:{
                pageAfterIn:function(page,id){
                    Template7.compile($$('#sign-up-confirm-telephone').html());                    
                }
            }
        },
        {
            path: '/results-list/:ad_categories_id?',
            url: 'results-list.html',
            on:{
                pageBeforeIn:function(page){
                   
                },
                pageAfterIn:function(event,page){                    
                    var params = page.route.params;   
                    if(typeof(params.ad_categories_id)!='undefined' && params.ad_categories_id>=1 && params.ad_categories_id<=5){
                        window.localStorage.setItem('ad_categories_id', params.ad_categories_id);    
                    }                   
                    setToolbarColor();
                    loadResults(params);
                    loadResultCategories();
                }
            }
        },
        {
            path:'/product-page/:id/',
            url:'product-page.html',
            on:{
                pageAfterIn:function(event,page){
                    var params = page.route.params;        
                    console.log(params);
                    loadResult(params.id);
                    setToolbarColor();
                }
            }
        },
        {
            path: '/home-categories/',
            url: 'home-categories.html',
        },
        {
            path:'/item-insert-form-thank-you/',
            url:'item-insert-form-thank-you.html',
            on:{
                pageAfterIn:function(event,page){
                 loadCategories();
                }
            }
        },
        {
            path:'/item-insert-form/',
            url:'item-insert-form.html',
            on:{
                pageAfterIn:function(event,page){
                    loadCategories();

                    $$(document).on('DOMNodeInserted','.pac-container' , function() {
                        $$('.pac-item, .pac-item span').addClass('no-fastclick');
                        }
                    );

                   
                }
            }
        }
    ],
    on: {
        init: function (e) {
            //do something after app initialized
            
            


        },
        pageInit: function (page,id) {
            //do something during page initialized
            if(pageCallbacks[page.name]){
                pageCallbacks[page.name](page);
            }
        },
        pageBeforeIn: function (page) {
            //do something before page init
            
        },
        pageAfterIn: function (page) {
            app.request.get(api_endpoint+'/api/text_contents',function(contents){
                contents = JSON.parse(contents);
                var context = {links:contents.data};
                console.log(context);
                var compiledTemplate = Template7.compile($$('#panelLinksTemplate').html());  
                $$('#panelLinks').html(compiledTemplate(context));
                $$('#panelLinksTemplate').remove();
                        
            })


             /*SUBSCRIBE FOR GENERIC NOTIFICATION ON USER*/
            
            var pusherGeneric = new Pusher('b3a0d0d3965356c99245', {
                            cluster: 'eu',
                            encrypted: true,
                        });
            var pageName = mainView.router.currentRoute.name;
            var channelGeneric = pusherGeneric.subscribe('chat');
            channelGeneric.bind('generic_'+window.localStorage.getItem('cms_user_id'), data => {

                 
            });
            /********************************************/

            $$('.sign-up-confirm-form-submit').on('click',function(){
                var verification_token = '';
                $$('.verify-token-cyphres input').each(function(){
                    verification_token += $$(this).val();
                })
                if(verification_token.length!=5){
                    alert('Il codice impostato non è valido.')
                }else{
                    var formData = app.form.convertToData('#sign-up-confirm-form');
                    formData.verification_token = verification_token;
                    app.request.post(api_endpoint+'/api/user-verify',formData,function(data){
                        data = JSON.parse(data);
                        switch(data.status){
                            case 0:
                                alert('Il codice inserito non è valido.');
                            break;
                            case 1:
                                console.log('loginsuccess');
                                window.localStorage.setItem('cms_user_id', data.user.id);
                                mainView.router.navigate('/categories/');
                            break;
                        }
                        console.log(data);
                    })
                }
            })



            $$('.verify-token-cyphres input').on('focus',function(){
                $$(this).val('');
            })

            $$('.verify-token-cyphres input').on('keyup',function(e){
                
                    $$(this).next('input').focus();
                
            })

            $$('.sign-up-verify').on('click',function(){
                signUpVerify(8)
            })

            $$('.sign-up-form-submit').on('click',function(){
                var formData = app.form.convertToData('#sign-up-form');
                app.request.post(api_endpoint+'/api/users_add_create', formData, function (data) {
                    data = JSON.parse(data);
                  
                  if(data.api_status==1){
                    console.log(data);
                    signUpVerify(data.id)
                  }else{
                    alert("Compila correttamente tutti i campi obbligatori!");
                  }
                });
            })
            
         
        },
        routerAjaxStart: function (xhr, options) {
            app.preloader.show();
        },
        routerAjaxComplete: function (xhr, options) {
            app.preloader.hide();
        }
    },
});

// Add main View
var mainView = app.views.create('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: true,
});



function signUpVerify(id){
    app.request.get(api_endpoint+'/api/users_get',{id:id},function(data){
        data = JSON.parse(data);
        data = data.data[0];
        console.log(data);
        mainView.router.navigate('/sign-up-confirm/',{
            context:{
                telephone:data.telephone
            }
        });
    })
}
// Export selectors engine











