<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCmsUserAddressColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            //
            $table->string('cms_user_full_address',100)->nullable();
            $table->string('cms_user_administrative_area_level_1',100)->nullable();
            $table->string('cms_user_administrative_area_level_2',100)->nullable();
            $table->string('cms_user_administrative_area_level_3',100)->nullable();
            $table->string('cms_user_locality',100)->nullable();
            $table->string('cms_user_postal_code',100)->nullable();
            $table->string('cms_user_route',100)->nullable();
            $table->string('cms_user_street_number',100)->nullable();
            $table->string('cms_user_country',100)->nullable();
            $table->string('cms_user_lat',100)->nullable();
            $table->string('cms_user_lng',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            //
        });
    }
}
