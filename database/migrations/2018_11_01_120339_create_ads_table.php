<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('ad_categories_id');
            $table->string('ad_title',100)->nullable();
            $table->string('ad_description',100)->nullable();
            #POINT AND DATE 1
            $table->dateTime('t1')->nullable();
            $table->string('p1_full_address',100)->nullable();
            $table->string('p1_administrative_area_level_1',100)->nullable();
            $table->string('p1_administrative_area_level_2',100)->nullable();
            $table->string('p1_administrative_area_level_3',100)->nullable();
            $table->string('p1_locality',100)->nullable();
            $table->string('p1_postal_code',100)->nullable();
            $table->string('p1_route',100)->nullable();
            $table->string('p1_street_number',100)->nullable();
            $table->string('p1_country',100)->nullable();
            $table->string('p1_lat',100)->nullable();
            $table->string('p1_lng',100)->nullable();
            #END POINT AND DATE 1
            #POINT AND DATE 2
            $table->dateTime('t2')->nullable();
            $table->string('p2_full_address',100)->nullable();
            $table->string('p2_administrative_area_level_1',100)->nullable();
            $table->string('p2_administrative_area_level_2',100)->nullable();
            $table->string('p2_administrative_area_level_3',100)->nullable();
            $table->string('p2_locality',100)->nullable();
            $table->string('p2_postal_code',100)->nullable();
            $table->string('p2_route',100)->nullable();
            $table->string('p2_street_number',100)->nullable();
            $table->string('p2_country',100)->nullable();
            $table->string('p2_lat',100)->nullable();
            $table->string('p2_lng',100)->nullable();
            #END POINT AND DATE 2
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
