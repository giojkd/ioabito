<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user-verify','AdminCmsUsersController@verifyUser');

Route::post('/sign-in-user','ApiUsersGetController@signInUser');

Route::get('/ads','AdminAdsController@resultsList');

Route::get('/categories','AdminAdCategoriesController@categoriesList');

Route::get('/chat','ChatController@newMessage');

Route::post('/new-message', 'PusherController@sendMessage');

Route::get('/chat-list/{cms_users_id}/','PusherController@chatList');

Route::get('/get-chats-id/{ads_id}/{cms_users_id}/','PusherController@getChatsId');

Route::get('/get-chat-messages/{chats_id}/{cms_users_id}/','PusherController@getChatMessages');