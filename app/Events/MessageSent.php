<?php

namespace App\Events;

use App\Chats;
use App\ChatMessage;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $chat;
    public $chat_message;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Chats $chat,ChatMessage $chat_message)
    {
        $this->chat = $chat;
        $this->chat_message = $chat_message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

     public function broadcastWith()
    {
        // This must always be an array. Since it will be parsed with json_encode()
        return [
            'chat' => $this->chat,
            'message' => $this->chat_message,
        ];
    }

     public function broadcastAs()
    {
        return 'chat';
    }

    public function broadcastOn()
    {
        return new Channel('chat');
    }
}
