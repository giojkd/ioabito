<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Vendor\Malhal\Geographical;

class Ads extends Model
{
    const LATITUDE  = 'p1_lat';
	const LONGITUDE = 'p1_lng';
	protected static $kilometers = true;



	public function scopeDistance($query, $latitude, $longitude)
    {
        $latName = 'p1_lat';
        $lonName = 'p1_lng';
        $query->select($this->getTable() . '.*');
        $sql = "((ACOS(SIN(? * PI() / 180) * SIN(" . $latName . " * PI() / 180) + COS(? * PI() / 180) * COS(" .
            $latName . " * PI() / 180) * COS((? - " . $lonName . ") * PI() / 180)) * 180 / PI()) * 60 * ?) as distance";       
            $query->selectRaw($sql, [$latitude, $latitude, $longitude, 1.1515 * 1.609344]);
       

        //echo $query->toSql();
        //var_export($query->getBindings());
        return $query;
    }

}
