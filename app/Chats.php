<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chats extends Model
{
    //
    protected $fillable = ['ads_id','cms_users_id']; 
    protected $table = 'chats';

    public function messages(){
    	return $this->hasMany(ChatMessage::class);
    }
}
