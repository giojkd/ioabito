<?php 
namespace App\Http\Controllers;

		use Session;
		use DB;
		use CRUDBooster;
		use App\Sms;
		use Illuminate\Http\Request;

		class ApiUsersGetController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "cms_users";        
				$this->permalink   = "users_get";    
				$this->method_type = "get";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		    public function signInUser(Request $request){

		    	$user = DB::table('cms_users')->where('telephone', $request->input('telephone'))->exists();
		    	if($user){
		    		$user = DB::table('cms_users')->where('telephone', $request->input('telephone'))->first();
		    		$verificationToken = rand (10000,99999);
		    		DB::table('cms_users')->where('id',$user->id)->update(['verification_token'=>$verificationToken]);
		    		$sms = new Sms([
		        	'message'=>'Ciao '.$user->name.', questo è il codice di conferma che stavi aspettando: '.$verificationToken,
		        	'recipients'=>[$request->input('telephone')]
		        	]);
		        	$sms->send();
		        	$return['message'] = "";
		        	$return['status'] = 1;
		    	}else{
		    		$return['message'] = "Il numero inserito non appartiene a nessun utente.";
		    		$return['status'] = 0;
		    	}
		    	
		    	return $return;
		    }

		}