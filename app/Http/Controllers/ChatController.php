<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MessageSent;

use App\Chats;
use App\ChatMessage;

class ChatController extends Controller
{

	public function print_rr($array){
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}

    public function newMessage(Request $request){
    	$chat = Chats::firstOrCreate(
    		['ads_id' => $request->input('ads_id')], ['cms_users_id' => $request->input('cms_users_id')]
		);
		$message = $chat->messages()->create([
			'message' => $request->input('message')
		]);
		echo 'broadcast';
		broadcast(new MessageSent($chat, $message))->toOthers();
    }
}
