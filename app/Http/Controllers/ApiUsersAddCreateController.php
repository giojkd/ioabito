<?php 
namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;
		use App\Sms;

		class ApiUsersAddCreateController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "cms_users";        
				$this->permalink   = "users_add_create";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
		        $postdata['date_of_birth'] = implode('-',array_reverse(explode('-',$postdata['date_of_birth'])));
		        $postdata['verification_token'] = rand (10000,99999);
		        $sms = [
		        	'message'=>'Ciao '.$postdata['name'].', questo è il codice di conferma che stavi aspettando: '.$postdata['verification_token'],
		        	'recipients'=>[$postdata['telephone']]
		        ];
		        
		        $sms = new Sms($sms);
		        $sms->send();
		        
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}