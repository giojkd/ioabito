<?php namespace App\Http\Controllers;

	use Session;
	
	use DB;
	use CRUDBooster;
	use Illuminate\Http\Request;
	use Geocoder\Laravel\ProviderAndDumperAggregator as Geocoder;
	use App\Ads;



	class AdminAdsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "ad_title";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "ads";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Category","name"=>"ad_categories_id","join"=>"ad_categories,category_name"];
			$this->col[] = ["label"=>"Title","name"=>"ad_title"];
			$this->col[] = ["label"=>"Description","name"=>"ad_description"];
			$this->col[] = ["label"=>"Time From","name"=>"t1"];
			$this->col[] = ["label"=>"Address From","name"=>"p1_full_address"];
			$this->col[] = ["label"=>"Time To","name"=>"t2"];
			$this->col[] = ["label"=>"Address To","name"=>"p2_full_address"];
			$this->col[] = ["label"=>"User","name"=>"cms_users_id","join"=>"cms_users,surname"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Category','name'=>'ad_categories_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'ad_categories,category_name'];
			$this->form[] = ['label'=>'Title','name'=>'ad_title','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Description','name'=>'ad_description','type'=>'textarea','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Time From','name'=>'t1','type'=>'datetime','validation'=>'date_format:Y-m-d H:i:s','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Address From','name'=>'p1_full_address','type'=>'text','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Administrative Area Level 1','name'=>'p1_administrative_area_level_1','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Administrative Area Level 2','name'=>'p1_administrative_area_level_2','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Administrative Area Level 3','name'=>'p1_administrative_area_level_3','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Locality','name'=>'p1_locality','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Postal Code','name'=>'p1_postal_code','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Route','name'=>'p1_route','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Street Number','name'=>'p1_street_number','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Country','name'=>'p1_country','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Lat','name'=>'p1_lat','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P1 Lng','name'=>'p1_lng','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Time To','name'=>'t2','type'=>'datetime','validation'=>'date_format:Y-m-d H:i:s','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Address To','name'=>'p2_full_address','type'=>'text','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Administrative Area Level 1','name'=>'p2_administrative_area_level_1','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Administrative Area Level 2','name'=>'p2_administrative_area_level_2','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Administrative Area Level 3','name'=>'p2_administrative_area_level_3','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Locality','name'=>'p2_locality','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Postal Code','name'=>'p2_postal_code','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Route','name'=>'p2_route','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Street Number','name'=>'p2_street_number','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Country','name'=>'p2_country','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Lat','name'=>'p2_lat','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'P2 Lng','name'=>'p2_lng','type'=>'hidden','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'User','name'=>'cms_users_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'cms_users,surname'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Category','name'=>'ad_categories_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'ad_categories,category_name'];
			//$this->form[] = ['label'=>'Title','name'=>'ad_title','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Description','name'=>'ad_description','type'=>'textarea','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Time From','name'=>'t1','type'=>'datetime','validation'=>'date_format:Y-m-d H:i','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Address From','name'=>'p1_full_address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Administrative Area Level 1','name'=>'p1_administrative_area_level_1','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Administrative Area Level 2','name'=>'p1_administrative_area_level_2','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Administrative Area Level 3','name'=>'p1_administrative_area_level_3','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Locality','name'=>'p1_locality','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Postal Code','name'=>'p1_postal_code','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Route','name'=>'p1_route','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Street Number','name'=>'p1_street_number','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Country','name'=>'p1_country','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Lat','name'=>'p1_lat','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P1 Lng','name'=>'p1_lng','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Time To','name'=>'t2','type'=>'datetime','validation'=>'required|date_format:Y-m-d H:i','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Address To','name'=>'p2_full_address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Administrative Area Level 1','name'=>'p2_administrative_area_level_1','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Administrative Area Level 2','name'=>'p2_administrative_area_level_2','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Administrative Area Level 3','name'=>'p2_administrative_area_level_3','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Locality','name'=>'p2_locality','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Postal Code','name'=>'p2_postal_code','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Route','name'=>'p2_route','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Street Number','name'=>'p2_street_number','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Country','name'=>'p2_country','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Lat','name'=>'p2_lat','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'P2 Lng','name'=>'p2_lng','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'User','name'=>'cms_users_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,surname'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        	 $this->load_js = array(
	        	"https://maps.googleapis.com/maps/api/js?key=AIzaSyA7_6qXx1LlpVA576SUsX9OPSdh0jw2WIo&libraries=places",
	        	"/assets/admin/js/ads_controller_js.js");
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function print_rr($array){
	    	echo '<pre>';
	    	print_r($array);
	    	echo '</pre>';
	    }

	    public function resultsList(Request $request){
	    	/*
	    	$user = DB::table('cms_users')->where('id',$input->request('cms_users_id_geo'))->first();
	    	$latitude = $user->cms_user_lat;
	    	$longitude = $user->cms_user_lng;
	    	*/
	    	$latitude = 43.769562;
	    	$longitude = 11.255814;
	    	$prod = 1.1515 * 1.609344;

	    	$results = 
	    	DB::table('ads')
	    	->select(
	    		'ads.*',
	    		'ads.id as ads_id',
	    		'ad_1.category_name as category_name',
	    		'ad_2.category_name as parent_category_name',
	    		'cms_users.name',
	    		'cms_users.telephone',
	    		DB::raw('( SELECT COUNT(*) FROM ads as ad_3 WHERE ad_3.cms_users_id = ads.cms_users_id) as count_user_ads'),
	    		DB::raw('((ACOS(SIN('.$latitude.' * PI() / 180) * SIN(p1_lat * PI() / 180) + COS('.$latitude.' * PI() / 180) * COS(p1_lat * PI() / 180) * COS(('.$longitude.' - p1_lng) * PI() / 180)) * 180 / PI()) * 60 * '.$prod.') as distance')
	    	)
	    	->leftJoin('ad_categories as ad_1','ad_1.id','=','ads.ad_categories_id')
	    	->leftJoin('ad_categories as ad_2','ad_2.id','=','ad_1.categories_id')
	    	->leftJoin('cms_users','cms_users.id','=','ads.cms_users_id')
	    	->orderBy('distance','ASC');

	    	if(!empty($request->input('ads_id'))){
	    		$results->where('ads.id',$request->input('ads_id'));	
	    	}

	    	if(!empty($request->input('ad_categories_id'))){
	    		$category = DB::table('ad_categories')->where('id',$request->input('ad_categories_id'))->first();
	    		
	    		if(!empty($category->categories_id)){
	    			
	    			$results->where('ads.ad_categories_id',$request->input('ad_categories_id'));	
	    		}else{
	    			$categories = DB::table('ad_categories')->where('categories_id',$request->input('ad_categories_id'));

	    			if($categories->exists()){
	    				$categoriesArray = $categories->get();
	    				foreach($categoriesArray as $category){
	    					$categories_[] = $category->id;
	    				}
	    				$results->whereIn('ads.ad_categories_id',$categories_);	
	    			}
	    		}
	    	}
	    	/*if(!empty($request->input('ad_categories_id'))){
	    		$results->where('ads.ad_categories_id',$request->input('ad_categories_id'));
	    	}*/

	    	$returnResults_ = $results->get();

	    	if(!empty($returnResults_)){
	    		foreach($returnResults_ as $rr){
	    			$rr->created_at = date('d-m-Y',strtotime($rr->created_at));
	    			$returnResults[] = $rr;
	    		}
	    	}


	    	$return['results'] = $returnResults;
	    	$return['resultsFound'] = (!empty($returnResults)) ? true : false;

	    	$categories = [];

	    	$categoriesTmp = DB::table('ad_categories')->get();
	    	foreach($categoriesTmp as $c){
	    		if(!empty($c->categories_id)){
	    			$categories[$c->categories_id]['children'][] = ['name'=>$c->category_name,'id'=>$c->id,'categories_id'=>$c->categories_id];
	    		}else{
	    			$categories[$c->id] = ['name'=>$c->category_name,'id'=>$c->id,'categories_id'=>$c->id];
	    		}
	    	}
	    	$return['categories'] = $categories;

	    	return $return;
	    }

	    public function testAdsDistance(){
	    	$query = Ads::distance(43.769562, 11.255814);
			$asc = $query->orderBy('distance', 'ASC')->get();
			echo '<pre>';
			print_r($asc);
	    }

	    public function geoCodeAds(Geocoder $geocoder){

	    	#$ads = 
	    		   
	    	$ads = DB::table('ads')
	    			->whereNotNull('updated_at')
	    			->where('geocoded_at','<','updated_at')
	    			->union(
	    				DB::table('ads')
	    		   		->whereNull('geocoded_at')
	    				)
	    			->get();

	    		   #echo '<pre>';
	   	
	    		   
	    	if(!empty($ads)){
	    		foreach($ads as $ad){
	    			
	    			$geo = $geocoder->geocode($ad->p1_full_address)->get()->first();
	    			
	    			

	    			
	    			
	    			
	    			if(!empty($geo)){
	    				DB::table('ads')
	    			->where('id',$ad->id)
	    			->update([
			    		'p1_lat'=>$geo->getCoordinates()->getLatitude(),
			    		'p1_lng'=>$geo->getCoordinates()->getLongitude(),
			    		'p1_administrative_area_level_1'=>$geo->getAdminLevels()->get(1)->getName(),
			    		'p1_administrative_area_level_2'=>$geo->getAdminLevels()->get(2)->getName(),
			    		#'p1_administrative_area_level_3'=>$geo->getAdminLevels()->get(3)->getName(),
			    		'p1_locality' => $geo->getLocality(),
			    		'p1_postal_code' => $geo->getPostalCode(),
			    		'p1_route' => $geo->getStreetName(),
			    		'p1_street_number' => $geo->getStreetNumber(),
			    		'p1_country' => $geo->getCountry()->getName(),
	    				'geocoded_at'=>date('Y-m-m H:i:s')
	    			]);

	    			if(!empty($ad->p2_full_address)){
	    				$geo = $geocoder->geocode($ad->p2_full_address)->get()->first();
		    			DB::table('ads')
		    			->where('id',$ad->id)
		    			->update([
			    		'p2_lat'=>$geo->getCoordinates()->getLatitude(),
			    		'p2_lng'=>$geo->getCoordinates()->getLongitude(),
			    		'p2_administrative_area_level_1'=>$geo->getAdminLevels()->get(1)->getName(),
			    		'p2_administrative_area_level_2'=>$geo->getAdminLevels()->get(2)->getName(),
			    		#'p2_administrative_area_level_3'=>$geo->getAdminLevels()->get(3)->getName(),
			    		'p2_locality' => $geo->getLocality(),
			    		'p2_postal_code' => $geo->getPostalCode(),
			    		'p2_route' => $geo->getStreetName(),
			    		'p2_street_number' => $geo->getStreetNumber(),
			    		'p2_country' => $geo->getCountry()->getName(),
	    			]);
	    			}
	    			}
	    			
	    			
	    		}
	    	}
	    	
	    	
	    	
	    	
	    	/*

	    	$result = [
	    		
	    	];
	    	
	    	print_r($result);
	    */
	    	#echo $geo[0]['resultType'][0];
	    }


	    //By the way, you can still create your own method in here... :) 


	}