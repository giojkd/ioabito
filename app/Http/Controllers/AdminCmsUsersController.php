<?php namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use DB;
use CRUDbooster;
use Geocoder\Laravel\ProviderAndDumperAggregator as Geocoder;

class AdminCmsUsersController extends \crocodicstudio\crudbooster\controllers\CBController {


	public function cbInit() {
		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->table               = 'cms_users';
		$this->primary_key         = 'id';
		$this->title_field         = "name";
		$this->button_action_style = 'button_icon';	
		$this->button_import 	   = FALSE;	
		$this->button_export 	   = FALSE;	
		# END CONFIGURATION DO NOT REMOVE THIS LINE
	
		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = array();
		$this->col[] = array("label"=>"Name","name"=>"name");
		$this->col[] = array("label"=>"Surname","name"=>"surname");
		$this->col[] = array("label"=>"Telephone","name"=>"telephone");
		$this->col[] = array("label"=>"Email","name"=>"email");
		$this->col[] = array("label"=>"Gender","name"=>"gender");
		$this->col[] = array("label"=>"Date of birth","name"=>"date_of_birth");
		$this->col[] = array("label"=>"Privilege","name"=>"id_cms_privileges","join"=>"cms_privileges,name");
		$this->col[] = array("label"=>"Photo","name"=>"photo","image"=>1);		
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = array(); 		
		$this->form[] = array("label"=>"Name","name"=>"name",'required'=>true,'validation'=>'required|alpha_spaces');
		$this->form[] = array("label"=>"Surname","name"=>"surname",'required'=>true,'validation'=>'required|alpha_spaces');
		$this->form[] = array("label"=>"Telephone","name"=>"telephone",'required'=>true,'validation'=>'required');
		$this->form[] = array("label"=>"Email","name"=>"email",'required'=>true,'type'=>'email','validation'=>'required|email|unique:cms_users,email,'.CRUDBooster::getCurrentId());		
		$this->form[] = array("label"=>"Gender","name"=>"gender","type"=>"select","dataenum"=>"Male;Female");
		$this->form[] = array("label"=>"Date of birth","name"=>"date_of_birth","type"=>"date");
		$this->form[] = array("label"=>"Photo","name"=>"photo","type"=>"upload","help"=>"Recommended resolution is 200x200px",'validation'=>'image|max:1000','resize_width'=>90,'resize_height'=>90);											
		$this->form[] = array("label"=>"Privilege","name"=>"id_cms_privileges","type"=>"select","datatable"=>"cms_privileges,name",'required'=>true);
		$this->form[] = array("label"=>"Verification token","name"=>"verification_token");		
		$this->form[] = array("label"=>"Status","name"=>"cms_user_status","type"=>"select","dataenum"=>"0|Disabled;1|Enabled");				
		$this->form[] = array("label"=>"Password","name"=>"password","type"=>"password","help"=>"Please leave empty if not change");

		#ADDRESS FIELDS

		$this->form[] = ['label'=>'Full Address','name'=>'cms_user_full_address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Administrative Area Level 1','name'=>'cms_user_administrative_area_level_1','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Administrative Area Level 2','name'=>'cms_user_administrative_area_level_2','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Administrative Area Level 3','name'=>'cms_user_administrative_area_level_3','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Locality','name'=>'cms_user_locality','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Postal Code','name'=>'cms_user_postal_code','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Route','name'=>'cms_user_route','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Street Number','name'=>'cms_user_street_number','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Country','name'=>'cms_user_country','type'=>'hidden','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Lat','name'=>'cms_user_lat','type'=>'hidden','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'cms_user Lng','name'=>'cms_user_lng','type'=>'hidden','width'=>'col-sm-10'];

		#END ADDRESS FIELDS

		# END FORM DO NOT REMOVE THIS LINE
				
		 $this->load_js = array(
	        	"https://maps.googleapis.com/maps/api/js?key=AIzaSyA7_6qXx1LlpVA576SUsX9OPSdh0jw2WIo&libraries=places",
	        	"/assets/admin/js/cms_users_controller_js.js");

	}

	public function getProfile() {			

		$this->button_addmore = FALSE;
		$this->button_cancel  = FALSE;
		$this->button_show    = FALSE;			
		$this->button_add     = FALSE;
		$this->button_delete  = FALSE;	
		$this->hide_form 	  = ['id_cms_privileges'];

		$data['page_title'] = trans("crudbooster.label_button_profile");
		$data['row']        = CRUDBooster::first('cms_users',CRUDBooster::myId());		
		$this->cbView('crudbooster::default.form',$data);				
	}

	public function verifyUser(Request $request){
		
		
		$userCheck = DB::table('cms_users')
				->where('telephone', $request->input('telephone'))
				->where('verification_token',$request->input('verification_token'))
				->exists();
		
		if($userCheck){
			$user = DB::table('cms_users')
				->where('telephone', $request->input('telephone'))
				->where('verification_token',$request->input('verification_token'))
				->first();
			return ['status'=>1,'user'=>$user];
		}else{
			return ['status'=>0];
		}
	}

	public function geocodeUsers(Geocoder $geocoder){
		$users = DB::table('cms_users')
	    			->whereNotNull('updated_at')
	    			->where('geocoded_at','<','updated_at')
	    			->union(
	    				DB::table('cms_users')
	    		   		->whereNull('geocoded_at')
	    				)
	    			->get();
	    if(!empty($users)){
	    	foreach ($users as $key => $user) {
	    		$geo = $geocoder->geocode($user->cms_user_full_address)->get()->first();
	    			
	    			

	    			
	    			
	    			
	    			if(!empty($geo)){
	    				DB::table('cms_users')
	    			->where('id',$user->id)
	    			->update([
			    		'cms_user_lat'=>$geo->getCoordinates()->getLatitude(),
			    		'cms_user_lng'=>$geo->getCoordinates()->getLongitude(),
			    		'cms_user_administrative_area_level_1'=>$geo->getAdminLevels()->get(1)->getName(),
			    		'cms_user_administrative_area_level_2'=>$geo->getAdminLevels()->get(2)->getName(),
			    		#'p1_administrative_area_level_3'=>$geo->getAdminLevels()->get(3)->getName(),
			    		'cms_user_locality' => $geo->getLocality(),
			    		'cms_user_postal_code' => $geo->getPostalCode(),
			    		'cms_user_route' => $geo->getStreetName(),
			    		'cms_user_street_number' => $geo->getStreetNumber(),
			    		'cms_user_country' => $geo->getCountry()->getName(),
	    				'geocoded_at'=>date('Y-m-m H:i:s')
	    			]);
	    	}
	    }
	}
}


}


