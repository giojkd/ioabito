<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Pusher\Pusher;
use App\Chats;
use App\Ads;
use App\ChatMessage;
use App\CmsUser;
use DB;

class PusherController extends Controller
{
    public function print_rr($array){
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }

    public function getChatsId($ads_id,$cms_users_id){
        $chat = Chats::firstOrCreate(['ads_id' => $ads_id,'cms_users_id'=>$cms_users_id]);
        return $chat->id;
    }

    public function getChatMessages($chats_id,$cms_users_id){
        if(
            DB::table('chat_messages')
            ->where('chats_id', $chats_id)
            ->exists()
        ){
            $messagesAux = 
            DB::table('chat_messages')
            ->leftJoin('cms_users','cms_users.id','=','chat_messages.cms_users_id')
            ->where('chats_id',$chats_id)->get();
            foreach($messagesAux as $message){
                if($message->cms_users_id == $cms_users_id){
                    $class = 'message-sent';
                }else{
                    $class = 'message-received';
                }
                $messages[] = [
                    'class'=> $class,
                    'text' => $message->message,
                    'name' => $message->name
                ];
            }
        
        }else{
            $messages = [];
        }
        return $messages;
    }
 
    public function chatList($cms_users_id){
        
        #chats on ads inserted by given user
        $chats_1 = DB::table('chats')
        ->select('ads.ad_title','ads.id as ads_id','chats.cms_users_id',DB::raw('(SELECT name FROM cms_users WHERE cms_users.id = ads.cms_users_id) as name'))
        ->leftJoin('ads','ads.id','=','chats.ads_id')
        ->leftJoin('cms_users','cms_users.id','=','ads.cms_users_id') #chat on ads insterted by given user
        ->where('ads.cms_users_id',$cms_users_id);
        
        ############################

        
        #chats opened by given user
        $chats = DB::table('chats')
        ->select('ads.ad_title','ads.id as ads_id','chats.cms_users_id',DB::raw('(SELECT name FROM cms_users WHERE cms_users.id = chats.cms_users_id) as name'))
        ->leftJoin('ads','ads.id','=','chats.ads_id')
        ->leftJoin('cms_users','cms_users.id','=','chats.cms_users_id') 
        ->where('chats.cms_users_id',$cms_users_id)
        ->union($chats_1)
        ->get();

        ####################################
        return $chats;
    }

    public function sendMessage(Request $request){

        $cms_users_id = $request->input('cms_users_id'); 
        $cms_users_id_msg = $request->input('cms_users_id_msg');
        $ads_id = $request->input('ads_id');
        $messageText = $request->input('message');

        $ads = Ads::find($ads_id);
        if($ads->cms_users_id == $cms_users_id){
            echo 'not allowed to open a chat with yourself';
        }else{
            $cmsUser = CmsUser::find($cms_users_id_msg);

            $chat = Chats::firstOrCreate(['ads_id' => $ads_id,'cms_users_id'=>$cms_users_id]);
            $chatMessage = new ChatMessage;
            $chatMessage->cms_users_id = $cms_users_id_msg;
            $chatMessage->chats_id = $chat->id;
            $chatMessage->message = $messageText;
            $chatMessage->save();

            $options = array(
                'cluster' => 'eu', 
                'encrypted' => true
            );

            $pusher = new Pusher(
                'b3a0d0d3965356c99245',
                '1fdd7efdc68e087ce521',
                '645754',
                $options
            );
            $message= [
                'name'=>$cmsUser->name,
                'cms_users_id' => $cms_users_id_msg,
                'text'=>$messageText
            ];
            $pusher->trigger('chat', 'chat_'.$chat->id, $message);

            $messageGeneric = [
                'text' => $messageText,
                'subTitle' => $ads->ad_title,
                'ads_id'=>$ads->id,
                'cms_users_id' => $chat->cms_users_id
            ];

            if($cms_users_id_msg != $ads->cms_users_id){
                $pusher->trigger('chat','generic_'.$ads->cms_users_id,$messageGeneric); #send notification to ads lister    
            }
            if($cms_users_id_msg != $chat->cms_users_id){
                $pusher->trigger('chat','generic_'.$chat->cms_users_id,$messageGeneric); #send notification to ads lister    
            }
            

            

            return $message;

        }
        
        
    }

    public function sendNotification()
    {
         
    }
}