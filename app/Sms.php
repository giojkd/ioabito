<?php 
namespace App;
use Request;
class Sms{

		var $message = '';
		var $recipients = [];

		function __construct($cfg) {    
			$this->message = $cfg['message'];
			$this->recipients = $cfg['recipients'];
		}

		public function loginSMS($username, $password) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,
				'https://api.skebby.it/API/v1.0/REST/login?username=' . $username .
				'&password=' . $password);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$response = curl_exec($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);

			if ($info['http_code'] != 200) {
				return null;
			}

			return explode(";", $response);
		}

		/**
		 * Sends an SMS message
		 */
		public function sendSMS($auth, $sendSMS) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,'https://api.skebby.it/API/v1.0/REST/sms');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-type: application/json',
				'user_key: ' . $auth[0],
				'Session_key: ' . $auth[1]
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sendSMS));
			$response = curl_exec($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);

			if ($info['http_code'] != 201) {
				return null;
			}

			return json_decode($response);
		}

		public function send(){


			$auth = $this->loginSMS(env('SKEBBY_USERNAME'), env('SKEBBY_PASSWORD'));
			$smsSent = $this->sendSMS($auth, array(
					"message" => $this->message,
					"message_type" => env('SKEBBY_MESSAGE_TYPE'),
					"returnCredits" => true,
					"recipient" => $this->recipients,
					"sender" => '+393452287526'
					#"sender" => env('SKEBBY_DEFAULT_SENDER'),
				));
		

			#DB::table('sms_logs')->insert([['content'=>$sms['message'],'recipients'=>json_encode($sms['recipients'])]]);
		}
}